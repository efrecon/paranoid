# Plugins Directory

This directory can contain any number of plugins.  All files ending
with the extension `.tcl` will be recognised as an external plugin.
They should contain procedure declarations that do *not* start with
`::` as the content of the each file will be loaded into a separate
namespace (where the ending name of the namespace will be the same as
the root name of the file).

Each plugin should implement a procedure called init which will be
called with the following arguments (in order):

* `slave`, the first argument will be the identifier of the slave
  interpreter that is being started up to run the on or off rules,
  depending on the action being taken.  Using this argument, the
  plugin should be able to expose additional (potentially dangerous)
  commands, and/or to load extra libraries into the interpreter to be
  able to executed any relevant extra control.

* `argv0` is the full resolved path to the main script.  This can be
  used to look for additional extensions and libraries for inclusion,
  for example.

* `argv` contains the list of options that were passed to the main
  program, this could be used to capture options in the plugins so as
  to take appropriate measures.

