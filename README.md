# Paranoid Lock

The idea behind Paranoid Lock application is to use USB keys both as
triggers to a set of rules (turning on or off devices) but also to
keep the information about the decisions to be taken stored on the USB
key itself.  Thus, the USB key becomes a "real" key, carrying vital
information about some IoT actions to perform whenever it is present
or absent.  The key and the host performing the actions are
automatically paired.

Rules are encrypted on the USB key, and rules to be exectuted when the
key is removed are cached (and encrypted) on the host so as to be able
to remain active for longer time periods.  Encryption uses a
combination of system-wide salting information, system-wide passwords
and the serial number that is inserted uniquely on each USB key at the
factory.  Losing a key has no digital consequences: to acquire
information that it carries, knowing the identifier of the host is
necessary (in addition to the serial number).  Similarly, rules that
are cached on the host are overwritten at each key insertion to avoid
durable tampering of rule cache.

Actions should either prevent their revert in the IoT context, force
their state on a regular basis, or act "deeper" down the accessible
abstractions to guarantee privacy.