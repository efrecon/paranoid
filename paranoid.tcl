set rootdir [file normalize [file dirname [info script]]]
lappend auto_path \
    [file normalize [file join $rootdir .. lib]] \
    [file normalize [file join $rootdir lib]]

package require utils
package require udev
package require md5

set prg_args {
    -cache      "%prgdir%/cache" "Directory for off-rules caching"
    -verbose    "* 4"            "Verbosity specification for internal modules"
    -h          ""               "Print this help and exit"
}

# Dump help based on the command-line option specification and exit.
proc ::help:dump { { hdr "" } } {
    if { $hdr ne "" } {
	puts $hdr
	puts ""
    }
    puts "Paranoid Lock"
    foreach { arg val dsc } $::prg_args {
	puts "\t${arg}\t$dsc (default: ${val})"
    }
    exit
}


# Did we ask for help at the command-line, print out all command-line
# options described above and exit.
if { [::utils::getopt argv -h] } {
    ::help:dump
}

# Extract list of command-line options into array that will contain
# program state.  The description array contains help messages, we get
# rid of them on the way into the main program's status array.
array set PND {}
foreach { arg val dsc } $prg_args {
    set PND($arg) $val
}
eval ::utils::verbosity $PND(-verbose);  # First pass for initial behaviour

foreach opt [array names PND -*] {
    ::utils::pushopt argv $opt PND
}

# Remaining args? Dump help and exit
if { [llength $argv] > 0 } {
    ::help:dump "[lindex $argv 0] is an unknown command-line option!"
}

# Local constants and initial context variables.
eval ::utils::verbosity $PND(-verbose);  # Second pass for life-long
					 # onwards behaviour

set startup "Starting [file rootname [file tail [info script]]] with args\n"
foreach {k v} [array get PND -*] {
    append startup "\t$k:\t$v\n"
}
::utils::debug INFO [string trim $startup]

# Now go for it! As everything is properly packaged, this is just a
# matter of requiring ruler, a package that will trigger the loading
# of all other necessary packages.
package require ruler
::ruler::init [string map [list %prgdir% $rootdir] $PND(-cache)]

vwait forever
