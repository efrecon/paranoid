# Cache

This directory is the directory that is holded off to safe
interpreters for storing data.  In other words, there will be, under
this directory, as many subdirectories as there has been recognised
USB keys.  Each subdirectory will contain data that they might wish to
save between sessions.  These subdirectories will also contain the
encrypted copies of the rules that should be executed when the key has
been removed.