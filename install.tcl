set rootdir [file normalize [file dirname [info script]]]
lappend auto_path [file join $rootdir .. lib] [file join $rootdir lib]

package require udev
package require utils
package require md5

set prg_args {
    -source     "%prgdir%/lamps" "Directory containing source (unencrypted) files"
    -dest       "/dev/sdc1"      "Device partition to write encrypted files to"
    -verbose    "* 4"            "Verbosity specification for internal modules"
    -h          ""               "Print this help and exit"
}

# Dump help based on the command-line option specification and exit.
proc ::help:dump { { hdr "" } } {
    if { $hdr ne "" } {
	puts $hdr
	puts ""
    }
    puts "Paranoid Lock"
    foreach { arg val dsc } $::prg_args {
	puts "\t${arg}\t$dsc (default: ${val})"
    }
    exit
}


# Did we ask for help at the command-line, print out all command-line
# options described above and exit.
if { [::utils::getopt argv -h] } {
    ::help:dump
}

# Extract list of command-line options into array that will contain
# program state.  The description array contains help messages, we get
# rid of them on the way into the main program's status array.
array set ITL {}
foreach { arg val dsc } $prg_args {
    set ITL($arg) $val
}
eval ::utils::verbosity $ITL(-verbose);  # First pass for initial behaviour

foreach opt [array names ITL -*] {
    ::utils::pushopt argv $opt ITL
}

# Remaining args? Dump help and exit
if { [llength $argv] > 0 } {
    ::help:dump "[lindex $argv 0] is an unknown command-line option!"
}

# Local constants and initial context variables.
eval ::utils::verbosity $ITL(-verbose);  # Second pass for life-long
					 # onwards behaviour

set startup "Starting [file rootname [file tail [info script]]] with args\n"
foreach {k v} [array get ITL -*] {
    append startup "\t$k:\t$v\n"
}
::utils::debug INFO [string trim $startup]


package require crypt
package require mount

# Find where the USB key is mounted
set dstdir [::mount::target $ITL(-dest)]
if { $dstdir eq "" } {
    ::utils::debug ERROR "Cannot find any mount point for $ITL(-dest)"
    exit
}
# Now create a directory pinpointed to the host identifier on that USB
# key.
set dstdir [file join $dstdir [::crypt::hostid]]
if { ![file isdirectory $dstdir] } {
    ::utils::debug NOTICE "Creating destination directory $dstdir"
    file mkdir $dstdir
}

# Get the serial number of the USB key and use it to copy and encrypt
# all the tcl sources found in the source directory into the
# pinpointed host directory on the USB key.
set serial [::udev::prop $ITL(-dest) ID_SERIAL_SHORT]
set srcdir [string map [list %prgdir% $rootdir] $ITL(-source)]
foreach f [glob -nocomplain -directory $srcdir -- *.tcl] {
    ::utils::debug INFO "Reading and encrypting $f"
    
    # Read content from source file and encrypt it.
    set fd [open $f]
    set dta [::crypt::encrypt [read $fd] $serial]
    close $fd

    # Dump encrypted content to file. Make sure we do this in binary
    # format, and to empty file content.
    set dpath [file join $dstdir [file tail $f]]
    ::utils::debug NOTICE "Writing encrypted version at $dpath"
    set fd [open $dpath {CREAT WRONLY BINARY TRUNC}]
    puts -nonewline $fd $dta
    close $fd
}

