namespace eval ::mac {}

proc ::mac::addr { { type "bound" } } {
    global tcl_platform

    switch -nocase -glob -- $type {
        "bound" {
            if { $tcl_platform(platform) eq "windows" } {
                return [Gather \
                            [concat [auto_execok ipconfig] /all] \
                            {(.*?):$} \
                            {.*ip.*?:\s*((\d{1,3}.){3}\d{1,3})} \
                            {{[Ee]thernet.*} {[Ww]ireless.*}} \
                            {}]
            } else {
                return [Gather \
                            [concat [auto_execok ifconfig] -a] \
                            {^(\w+\d+)} \
                            {^(inet\s+addr:|inet\s+)((\d{1,3}.){3}\d{1,3})} \
                            {{eth\d+} {wlan\d+} {en\d+}} \
                            {}]
            }
        }
        "eth*" {
            if { $tcl_platform(platform) eq "windows" } {
                return [Gather \
                            [concat [auto_execok ipconfig] /all] \
                            {(.*?):$} \
                            "" \
                            {{[Ee]thernet.*}} \
                            {{[Bb]luetooth}}]
            } else {
                return [Gather \
                            [concat [auto_execok ifconfig] -a] \
                            {^(\w+\d+)} \
                            "" \
                            {{eth\d+} {en\d+}} \
                            {}]
            }
        }
        "wire*" -
        "wifi" {
            if { $tcl_platform(platform) eq "windows" } {
                return [Gather \
                            [concat [auto_execok ipconfig] /all] \
                            {(.*?):$} \
                            "" \
                            {{[Ww]ireless.*}} \
                            {}]
            } else {
                return [Gather \
                            [concat [auto_execok ifconfig] -a] \
                            {^(\w+\d+)} \
                            "" \
                            {{wlan\d+} {en\d+}} \
                            {}]
            }
        }
    }
}

################################
## Local Procedures
################################

proc ::mac::Gather { cmd iface_ptn ipv4_ptn allow deny } {
    set macs {}

    set section {}
    if { [catch {eval [linsert $cmd 0 exec]} res] == 0 } {
        foreach l [split $res "\n\r"] {
            if { [string trim $l] ne "" } {
                if { ![string is space [string index $l 0]] } {
                    set mac [Section $iface_ptn $ipv4_ptn $allow $deny $section]
                    if { $mac ne "" } {
                        lappend macs $mac
                    }
                    set section {}
                    lappend section $l
                } else {
                    lappend section [string trim $l]
                }
            }
        }
        set mac [Section $iface_ptn $ipv4_ptn $allow $deny $section]
        if { $mac ne "" } {
            lappend macs $mac
        }
    }
    return $macs
}


proc ::mac::Section { iface_ptn ipv4_ptn allow deny section } {
    if { [llength $section] > 0 } {
        set mac_ptn {([0-9a-fA-F][0-9a-fA-F][\-:]){5}[0-9a-fA-F][0-9a-fA-F]}
        set mac ""
        set ip4 ""
        set iface ""
        set lno 0
        foreach s $section {
            if { $lno == 0 } {
                if { [regexp $iface_ptn $s - i] } {
                    set iface $i
                }
            }
            if { [regexp $mac_ptn $s m] } {
                set mac $m
            }
            if { $ipv4_ptn ne "" && [regexp -nocase -- $ipv4_ptn $s - - i] } {
                set ip4 $i
            }
            if { ($ip4 ne "" || $ipv4_ptn eq "" ) \
                     && $mac ne "" && $iface ne "" } {
                foreach ptn $allow {
                    if { [regexp $ptn $iface] } {
                        set denied 0
                        foreach ptn $deny {
                            if { [regexp $ptn $iface] } {
                                set denied 1
                                break
                            }
                        }
                        if { ! $denied } {
                            return $mac
                        }
                    }
                }
            }
            incr lno
        }
    }
    return ""
}

package provide mac 0.1
