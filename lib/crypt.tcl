##################
## Module Name     --  crypt.tcl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##     This package provides encryption and decryption services and
##     host identification services.
##
## Commands Exported:
##      hostid   (Unique) identification of host machine
##      encrypt  Encrypt data
##      decrypt  Decrypt data
##################
package require aes
package require md5
package require base64

package require mac
package require mount
package require disk

namespace eval ::crypt {
    # Encapsulates variables global to this namespace under their own
    # namespace, an idea originating from http://wiki.tcl.tk/1489.
    # Variables which name start with a dash are options and which
    # values can be changed to influence the behaviour of this
    # implementation.
    namespace eval vars {
	# String to use to bind together MAC address information and
	# (first) disk information when generating host identifier.
	variable -binder  "--"
	# Where should the "first" disk be mounted on the filesystem
	# to be considered as... the main disk.
	variable -mount   "/"
	# Salt to add to passwords to complexify encryption, can be in
	# binary form or base64 encoded.
	variable -salt    "7aqWN1B5MXZLvP7iqtixtfzLZ6GCYUcos06+L1QliAs="
	# Initialisation vector to use for AES encryption, can be in
	# binary form or base64 encoded.
	variable -iv      "5p2KCUBzwcuH8OQ1+fntTQ=="
	variable id       ""
    }
}

# ::crypt::hostid -- Return a unique identifier for the host machine
#
#       Returns a unique identifier for the host machine.  The
#       identifier is based on the MAC address of the first available
#       ethernet adapter, combined with the linux unique identifier of
#       the main disk, i.e. the device partition that is mounted on /.
#       These strings are combined and an MD5 hash is returned so as
#       to provide the unique host identifier.
#
# Arguments:
#       None.
#
# Results:
#       Unique immutable identifier of the host, as long as its
#       hardware configuration does not change.
#
# Side Effects:
#       Will run commands to detect hardware.
proc ::crypt::hostid {} {
    # If nothing in cache, explore hardware, otherwise return value in
    # cache.
    if { $vars::id eq "" } {
	set id [Mac];              # Get the main MAC address
	append id ${vars::-binder};# Combine with string binder
	append id [Disk];          # Append identifier of main disk
	set vars::id [::md5::md5 -hex $id]
	::utils::debug NOTICE "Actively generated unique identifier\
                               $vars::id for host"
    }
    return $vars::id
}


# ::crypt::encrypt -- Encrypt data
#
#       AES encrypt data, we use internal salting and complex
#       initialisation vectors to improve the quality of the
#       encryption.
#
# Arguments:
#	data	Data to encrypt
#	paswd	Password to encrypt with, will be used to form AES key
#
# Results:
#       Encrypted data.
#
# Side Effects:
#       None.
proc ::crypt::encrypt {data {paswd ""}} {
    append data [string repeat \\0 16]
    set len [expr {([string length $data]/16)*16}]
    return [::aes::aes \
		-dir encrypt \
		-key [Password $paswd] \
		-iv [Vector] \
		-- \
		[string range $data 0 [expr {$len-1}]]]
}


# ::crypt::decrypt -- Decrypt data
#
#       AES decrypt data, we use internal salting and complex
#       initialisation vectors to improve the quality of the
#       encryption.
#
# Arguments:
#	data	Data to decrypt
#	paswd	Password to decrypt with, will be used to form AES key
#
# Results:
#       Encrypted data.
#
# Side Effects:
#       None.
proc ::crypt::decrypt {data {paswd ""}} {
    set data [::aes::aes \
		  -dir decrypt \
		  -key [Password $paswd] \
		  -iv [Vector] \
		  -- \
		 $data]
    return [string trimright $data \\0]
}


###############
## Internal Procedures
###############

# ::crypt::Mac -- MAC address of first ethernet interface
#
#       Return the MAC address of the first ethernet interface.  We
#       arrange for the MAC address to be in upper case and not
#       contain separators.
#
# Arguments:
#       None.
#
# Results:
#       Uppercase address.
#
# Side Effects:
#       None.
proc ::crypt::Mac {} {
    set mac [lindex [::mac::addr ethernet] 0]
    ::utils::debug INFO "Main MAC address is $mac"
    return [string toupper [string map [list ":" ""] $mac]]
}


# ::crypt::Disk -- Unique identifier of main disk
#
#       Return the unique identifier (as of Linux) of the main disk in
#       the system, i.e. the disk that is mounted on /, a mountpoint
#       that can be changed by changing the vars::-mount variable.
#
# Arguments:
#	arg1	descr
#	arg2	descr
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::crypt::Disk {} {
    global PND

    # Find the mount point of the main partition, i.e. /.  Really we
    # look for $vars::-mount, but this variable should really contain
    # / and nothing else. It's kept as an option just in case.
    set mnt [::mount::origin ${vars::-mount}]
    if { $mnt ne "" } {
	set mnt [file normalize [::disk::resolve $mnt]]
    } else {
	::utils::debug WARN "Cannot find origin or mount point ${vars::-mount}"
	return ""
    }

    # Now, look among the known disks for the one which identifier
    # leads to the same device as the one that is mounted on the /
    # partition.  This will be the unique identifier of our main disk.
    return [::disk::find id $mnt]

    return ""
}


# ::crypt::Salt -- Salt string to complexify a password
#
#       This procedure salts an incoming string using the internal
#       salt to mask away passwords and sensible information.  The
#       resulting string will have the same length as the salt and is
#       the result of byte by byte composition of both strings.  If
#       the incoming string was shorter than the length of the salt,
#       it will be shortened.  If it was longer, it will be cut and
#       the remaining bytes will be ignored.
#
# Arguments:
#	str	Incoming string to be salted.
#
# Results:
#       Salted string of length fixed by length of internal salt.
#
# Side Effects:
#       None.
proc ::crypt::Salt { str } {
    # Base64 decode internal salt, or take it raw from the variable.
    if { [string index ${vars::-salt} end] eq "=" } {
	set salt [::base64::decode ${vars::-salt}]
    } else {
	set salt ${vars::-salt}
    }
    
    # Salt the incoming string, the resulting string will be exactly
    # of the length of the salt and is the result of composing the
    # bytes from both strings.
    set salt_len [string length $salt]
    set str_len [string length $str]
    set salted ""
    set j 0
    for {set i 0} {$i<$salt_len} {incr i} {
	# Compute salt character at index
	set salt_b [string index $salt $i]
	# Find password character at current string index.  If empty,
	# we've reached the end of the string, force a restart of the
	# string index to loop as many times as necessary through the
	# password.
	set str_b [string index $str $j]
	if { $str_b eq "" } {
	    set j 0
	    set str_b [string index $str $j]
	} else {
	    incr j
	}
	# If we still haven't a character from the incoming string,
	# then it was probably empty, so make this the character from
	# the salt only.
	if { $str_b eq "" } {
	    append salted $salt_b
	} else {
	    set cplx [expr {([scan $salt_b %c]+[scan $str_b %c])%256}]
	    append salted [format %c $cplx]
	}
    }

    return $salted
}


# ::crypt::Vector -- Initialisation vector.
#
#       Return an initialisation vector to be used for AES.  Convers
#       with base64 if necessary.
#
# Arguments:
#       None.
#
# Results:
#       Initialisation vector for AES cbc encryption
#
# Side Effects:
#       None.
proc ::crypt::Vector {} {
    # Base64 decode internal vector, or take it raw from the variable.
    if { [string index ${vars::-iv} end] eq "=" } {
	return [::base64::decode ${vars::-iv}]
    } else {
	return ${vars::-iv}
    }
}


# ::crypt::AllASCII -- Are all characters pure ASCII chars?
#
#       Detects if all characters are below 128 in a string
#
# Arguments:
#	str	Incoming string
#
# Results:
#       1 if they are, 0 otherwise.
#
# Side Effects:
#       None.
proc ::crypt::AllASCII { str } {
    foreach c [split $str ""] {
	if { [scan $c %c] > 127 } {
	    return 0
	}
    }
    return 1
}


# ::crypt::Password -- Good password
#
#       This procedure makes a good password out of a ... good(?)
#       password.  If the script is "plain english" it combines
#       characters two by two to generate a more complex string, and
#       then salt the password with the internal salt to complexifies
#       it further.
#
# Arguments:
#	passwd	Incoming password
#	combine	Should we combine characters two-by-two in weak strings?
#
# Results:
#       Salted string that can be used as the AES key.
#
# Side Effects:
#       None.
proc ::crypt::Password { passwd {combine 1}} {
    if { [string is true $combine] && [AllASCII $passwd] } {
	# Use pairs of characters to "compress" the password and
	# render it harder to guess, especially when concatenated to
	# the salt.
	set thepass ""
	for { set i 0 } {$i < [string length $passwd]} { incr i 2} {
	    set b1 [string index $passwd $i]
	    set b2 [string index $passwd [expr {$i+1}]]
	    append preamble [format %c [expr {[scan $b1 %c]+[scan $b2 %c]}]]
	}
    } else {
	set thepass $passwd
    }

    # Salt password with internal salt, ensuring a key of proper
    # length for AES encryption/decryption
    return [string range [Salt $thepass] 0 31]
}

package provide crypt 0.1
