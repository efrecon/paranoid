##################
## Module Name     --  keyw.tcl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##     This provides for USB keys lifecycle following from insertion
##     to removal.  Implementation is based on top of the udev
##     library, it supposes that the underlying linux system will
##     mount the key to a system-wide entry point on insertion.
##
## Commands Exported:
##      handler   Arrange for callbacks for matching USB keys.
##################
package require udev
package require mount

namespace eval ::keyw {
    # Encapsulates variables global to this namespace under their own
    # namespace, an idea originating from http://wiki.tcl.tk/1489.
    # Variables which name start with a dash are options and which
    # values can be changed to influence the behaviour of this
    # implementation.
    namespace eval vars {
	# Number of times we will try finding a matching USB as
	# mounted and ready to be used.
	variable -count   20
	# Period between checks for existence and mounting of USB
	# keys, in milliseconds.
	variable -period  300
	variable monitor  ""
	variable handlers {}
    }
}


# ::keyw::handler -- Register key callback
#
#       Arrange for a command to be called every time a matching USB
#       key is inserted or removed.  Removed callbacks will only be
#       called for keys that have first been inserted and discovered
#       by this library.  The command will be called with the
#       following additional arguments: operation (the string INSERT
#       or REMOVE), the serial number of the USB key, the
#       corresponding linux device path and, finally, where it
#       actually is mounted at.
#
# Arguments:
#	cmd	Command to register as a callback.
#	ptn	Pattern to match against the serial number of the key.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::keyw::handler { cmd {ptn *} } {
    # Create a general udev monitor if we don't have one yet
    if { $vars::monitor eq "" } {
	set vars::monitor [udev monitor]
    }

    # Start listening for the pattern, i.e. for USB keys that serial
    # number match the pattern.
    $vars::monitor handler [namespace current]::Dispatch \
	ACTION add ID_SERIAL_SHORT $ptn SUBSYSTEM usb

    # Remember the command to call on insertion and removals.
    lappend vars::handlers $cmd
}


#################
## Procedures private to the implementation!
#################


# ::keyw::Trigger -- Trigger command callback
#
#       Trigger all relevant handlers on insertion or removal of a USB
#       key.
#
# Arguments:
#	op	Operation to mediate (INSERT or REMOVE)
#	serial	Serial number of key.
#	dev	Device path where it is at
#	mount	Mountpoint (empty will look for it).
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::keyw::Trigger { op serial dev {mount ""}} {
    if { $mount eq "" } {
	set mount [::mount::target $dev]
    }

    ::utils::debug DEBUG \
	"Dispatching $op / $serial ($dev -- $mount) to handlers"
    foreach cmd $vars::handlers {
	if { [catch {eval [linsert $cmd end $op $serial $dev $mount]} err] } {
	    ::utils::debug WARN "Cannot callback $cmd: $err"
	}
    }
}


# ::keyw::Mounted -- Mount and presence detection
#
#       Detect if a USB is properly mounted and accessible, trigger
#       callback when it is ready.  Arrange for retrying a finite
#       number of times.
#
# Arguments:
#	id	(Linux) unique identifier of the USB key 
#	serial	Serial number of the key
#	count	Number of attempts we should make in total
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::keyw::Mounted { id serial count } {
    ::utils::debug DEBUG "Waiting for mounting of $id"

    # Wait until some of the partitions available on the USB key have
    # been discovered, we only care for the first partition.  Give up
    # if we've reached the maximum number of attempts.
    set dev [lindex [lsort [::disk::devices id *${id}*part*]] 0]
    if { $dev eq "" } {
	incr count -1
	if { $count > 0 } {
	    after ${vars::-period} \
		[list [namespace current]::Mounted $id $serial $count]
	} else {
	    ::utils::debug WARN "Maximum number of iterations reached\
                                 giving up on $serial!"
	}
	return
    }

    # Wait until the first available partition has properly been
    # mounted somewhere so external callers will be able to use the
    # USB key.  Give up if we've reached the maximum number of
    # attempts.
    ::utils::debug INFO "Inserted disk $serial available at: $dev,\
                          waiting for mount point"
    set mnt [::mount::target $dev]
    if { $mnt eq "" } {
	incr count -1
	if { $count > 0 } {
	    after ${vars::-period} \
		[list [namespace current]::Mounted $id $serial $count]
	} else {
	    ::utils::debug WARN "Maximum number of iterations reached\
                                 giving up on $serial!"
	}
	return
    }	

    # Now trigger the insertion callback with all the available
    # information, and start listening for USB removal for that very
    # key.
    ::utils::debug NOTICE "Disk $serial available as $mnt ($dev)\
                           waiting for removal"
    Trigger INSERT $serial $dev $mnt
    $vars::monitor handler [namespace current]::Dispatch \
	ACTION remove ID_SERIAL_SHORT $serial DEVNAME $dev
}


# ::keyw::Dispatch -- udev-compatible dispatcher
#
#       This procedure is registered as a callback to the udev
#       library.  It will relays removal and insertion detection and
#       handlers.
#
# Arguments:
#	e	udev event
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::keyw::Dispatch { e } {
    switch [$e get ACTION] {
	"add" {
	    after ${vars::-period} \
		[list [namespace current]::Mounted \
		     [$e get ID_SERIAL] \
		     [$e get ID_SERIAL_SHORT] \
		     ${vars::-count}]
	}
	"remove" {
	    ::utils::debug NOTICE \
		"[$e get ID_SERIAL_SHORT] removed from USB port"
	    Trigger REMOVE [$e get ID_SERIAL_SHORT] [$e get DEVNAME]
	}
    }
}

package provide keyw 0.1
