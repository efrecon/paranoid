##################
## Module Name     --  mount.tcl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##    This library provides a higher-level interface to mount
##    information on a running Linux system.
##
## Commands Exported:
##      origin   Origin (device) of an existing (and alive) mountpoint
##      target   Discover mountpoint of a device path
##################
package require utils

namespace eval ::mount {
    # Encapsulates variables global to this namespace under their own
    # namespace, an idea originating from http://wiki.tcl.tk/1489.
    # Variables which name start with a dash are options and which
    # values can be changed to influence the behaviour of this
    # implementation.
    namespace eval vars {
	# Path to the mount command, since it is the information that
	# we are relaying.
	variable -mount     "mount"
    }
}

# ::mount::origin -- Origin of an existing mountpoint
#
#       Given a mountpoint, return the origin device of that
#       mountpoint, i.e. which device is actually mounted on that
#       directory.
#
# Arguments:
#	dst	Path to mountpoint
#
# Results:
#       Return the origin device of the mountpoint, empty string if
#       not found.
#
# Side Effects:
#       Calls the mount command.
proc ::mount::origin { dst } {
    set mnt "";    # Will contain the mount point, empty for not found

    # Construct pipe command.
    set cmd "|"
    append cmd [auto_execok ${vars::-mount}]

    # Execute the command and read line by line
    set fd [open $cmd]
    fconfigure $fd -buffering line
    while {![eof $fd]} {
	set l [gets $fd]
	if { [lindex $l 2] eq $dst } {
	    set mnt [lindex $l 0]
	    break
	}
    }
    close $fd

    if { $mnt ne "" } {
	::utils::debug INFO "$mnt mounted on $dst"
    } else {
	::utils::debug WARN "$dst not mounted"
    }

    return $mnt
}

# ::mount::target -- Discover mount target of device
#
#       Given a device path, return where it is mounted at, i.e. its
#       mount point.
#
# Arguments:
#	src	Path to device.
#
# Results:
#       Return the mountpoint of the device, empty string if not
#       found.
#
# Side Effects:
#       None.
proc ::mount::target { src } {
    set mnt "";    # Will contain the mount point, empty for not found

    # Construct pipe command.
    set cmd "|"
    append cmd [auto_execok ${vars::-mount}]

    # Execute the command and read line by line
    set fd [open $cmd]
    fconfigure $fd -buffering line
    while {![eof $fd]} {
	set l [gets $fd]
	if { [lindex $l 0] eq $src } {
	    set mnt [lindex $l 2]
	    break
	}
    }
    close $fd

    if { $mnt ne "" } {
	::utils::debug INFO "$src mounted on $mnt"
    } else {
	::utils::debug WARN "$src not mounted"
    }

    return $mnt
}

package provide mount 0.1
