##################
## Module Name     --  ruler
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##     This library implements the core of the paranoid concepts: it
##     watches for insertions of USB keys and will, upon insertion,
##     run the rules that are pinpointed to this host on the USB key.
##     Rules are decrypted using the serial number of the key itself,
##     and the off rules are copied to a local cache on the host disk
##     so as to be available even though the USB key is not present in
##     the host.  Copying uses an internal password for encryption to
##     avoid eavesdropping.
##
##     Rules are regular Tcl scripts, but are executed in a very
##     restricted so-called safe interpreter.  This interpreter is
##     constructed on top of the Safe Base (see
##     http://www.tcl.tk/man/tcl8.5/TclCmd/safe.htm) with the
##     following differences.  All path accessible to the auto_path of
##     the main paranoid program are made available for sourcing as
##     part of the interpreter (this might change and be restricted
##     further in the future).  Insertion and removal scripts that are
##     run in the interpreter are given a directory to be run in, they
##     can be able to run write/read files from this directory (and
##     its subdirectories) if extensions wishes them to (see below).
##
##     Extensions (think plugins) can be registered to give more power
##     to slave and safe interpreters.  These should be placed in the
##     directory pointed at by the variable vars::-extensions.  They
##     should contain a procedure called init (without namespace
##     specification!) that will be called whenever a slave
##     interpreter is created.  The procedure itself will be isolated
##     within the main program in a separate namespace, one for each
##     registered extension.  The procedure will be called with the
##     following arguments, in order: the identifier of the slave
##     interpreter to enhance, the path to the main program and the
##     arguments to the main program.
##
## Commands Exported:
##      init    Start watching for USB key insertion
##################

## TODO: also use the identifier of the host to encrypt data on the
## USB key, and stop using the host identifier to create the
## directory, but rather use the UUID of the main disk.

package require utils
package require crypt
package require keyw

namespace eval ::ruler {
    # Encapsulates variables global to this namespace under their own
    # namespace, an idea originating from http://wiki.tcl.tk/1489.
    # Variables which name start with a dash are options and which
    # values can be changed to influence the behaviour of this
    # implementation.
    namespace eval vars {
	# List of rules (filenames) to execute from the key whenever
	# it is inserted.
	variable -insertion  {on}
	# List of rules (filenames) to copy (and later execute) from
	# the key whenever it is removed.
	variable -removal    {off}
	# Path to where extensions are located and loaded from.
	variable -extensions "%prgdir%/exts/*.tcl"
	# Entry point in extensions to call with the identifier of
	# each safe interpreter created.
	variable -entry      "init"
	# Internal password to use when encrypted data into the local
	# cache.
	variable -password   "Z%;325_ixwW4!0V04Re8aJ+3;s-%Q6ta"
	variable cache       ""
	variable uuids       {}
	variable prgdir      ""
    }
}

# ::ruler::init -- Initialise and start listening for USB insertion/removal
#
#       Initialisases this library and start arranging to receive USB
#       insertion and removal callbacks.
#
# Arguments:
#	cache	(local) root of directory to store off rules
#
# Results:
#       None.
#
# Side Effects:
#       Will react to insertion/removal of USB keys, run rules, copy
#       rules, etc.
proc ::ruler::init { cache } {
    set vars::cache $cache
    if { ![file isdirectory $vars::cache] } {
	::utils::debug NOTICE "Creating cache directory at $vars::cache"
	file mkdir $vars::cache
    }

    ::keyw::handler [namespace current]::Watch
}


################
## Procedures private to the implementation
################

# ::ruler::Extend -- Extend a safe slave interpreter
#
#       Given a freshly created safe slave interpreter, call all the
#       existing registered extensions entrypoints with that
#       interpreter as a parameter so as to make sure that it can
#       perform all necessary operations in the insertion/removal
#       rules.
#
# Arguments:
#	slave	Identifier of the safe slave interpreter.
#
# Results:
#       Return a list of even lenght where the first elements are the
#       fully-qualified namespace extension entry points that were
#       successfully called and the second elements their results.
#
# Side Effects:
#       Call extensions entrypoints with slave interp as a parameter.
proc ::ruler::Extend { slave } {
    set triggers {}
    # Give away all what the extensions want to be able to access to
    # the slave interpreter.
    foreach child [namespace children [namespace current]::extensions] {
	::utils::debug DEBUG \
	    "Applying extension [namespace tail $child] on $slave"
	# Access the init command of each extension and call it with
	# the name of the slave, but also program specific context.
	set cmd ${child}::${vars::-entry}
	if { [info procs $cmd] > 0 } {
	    if { [catch {eval [list $cmd $slave $::argv0 $::argv]} res] } {
		::utils::debug WARN "Could not execute $cmd: $res"
	    } else {
		lappend triggers $cmd $res
		::utils::debug NOTICE "Initialised extension\
                                       [namespace tail $child] for $slave"
	    }
	} else {
	    ::utils::debug WARN "Loaded extension has not proper entry point,\
                                 should have a procedure called ${vars::-entry}"
	}
    }
    return $triggers
}


# ::ruler::Safe -- Create a slave interpreter
#
#       Creates and initialises a safe slave interpreter for a USB
#       key.  As there should only be one interpreter at a time, this
#       procedure will also kill and remove any interpreter for that
#       same key.  Arrange for the current directory in that
#       interpreter to be the one passed as a parameter.
#
# Arguments:
#	serial	Serial number of USB key.
#	cdir	Unique cache directory for that key.
#	start	Tag to add to key name to generate interp name
#	kill	Same tag as above, but for the interp to remove and kill.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::ruler::Safe { serial cdir start { kill "" } } {
    # Remember main program hosting directory to be able to find
    # extensions
    if { $vars::prgdir eq "" } {
	set vars::prgdir [file dirname [file normalize $::argv0]]
	::utils::debug DEBUG "Remembered main program dir. as $vars::prgdir"
    }

    # Use the presence of a child namespace called extensions as the
    # marker for if we have initialise the extensions or not.  If not
    # yet, arrange for all extensions to have code isolated under
    # their own sub namepace.
    if { ![namespace exists [namespace current]::extensions] } {
	# Create the "marker" namespace, i.e. the one that will
	# contain one subnamespace for each extension.
	namespace eval extensions {}

	# Find all files under the -extensions directory and source
	# their content into a subnamespace that will have the same
	# name as the name of the file.  This is potentially
	# dangerous, but at least we have some sort of isolation.
	set exts [string map [list %prgdir% $vars::prgdir] ${vars::-extensions}]
	foreach f [glob -nocomplain -- $exts] {
	    set ext [file rootname [file tail $f]]
	    namespace eval extensions::$ext source [file normalize $f]
	}
    }


    # Kill the other execution context, we should only have one at a
    # time running.
    if { $kill ne "" && [interp exists ${serial}-$kill] } {
	::safe::interpDelete ${serial}-${kill}
	::utils::debug INFO "Deleted execution context ${serial}-$kill"
    }

    # Create safe interpreter and share stdout and stderr with it.  We
    # might want not to share stderr, but capture the output of
    # utils::debug back into the main interpreter instead?
    #::utils::debug DEBUG "Forcing current directory to $prgdir"
    cd $vars::prgdir
    set path [::safe::AddSubDirs $::auto_path]
    set slave [::safe::interpCreate ${serial}-${start} -accessPath $path]
    #::safe::setLogCmd ::utils::debug 4

    # Extend the slave, if relevant and make sure that rules will be
    # able to access part of the cache directory.
    Extend $slave
    $slave invokehidden cd $cdir

    ::utils::debug INFO "Created execution context $slave"
    return $slave
}


# ::ruler::Files -- Return (rule) files in directory
#
#       This procedure looks for a list of rule files in a directory.
#       The procedure automagically appends the .tcl extension to the
#       names of the files and also attempts to find those amended
#       names in the directory.  First match will win.  Not finding a
#       file is not an error.
#
# Arguments:
#	dir     Container directory
#	sources	List of (core) filenames to look for
#
# Results:
#       Return list of existing filename according to the incoming
#       list of possible sources.
#
# Side Effects:
#       None.
proc ::ruler::Files { dir {sources {}}} {
    set files {}

    foreach src $sources {
	set fname [file join $dir $src]
	if { ![file exists $fname] } {
	    set fname [file join $dir ${src}.tcl]
	}

	if { [file exists $fname] } {
	    lappend files $fname
	}
    }
    ::utils::debug DEBUG "Files at $dir are: $files" 
    return $files
}


# ::ruler::Source -- Source files in (safe) interpreter
#
#       Source the list of files passed as a parameter in a safe
#       interpreter.  The content of the files is decrypted using the
#       provided password before being sent to the interpreter for
#       evaluation.
#
# Arguments:
#	slave	Identifier of the slave (safe) interpreter
#	paswd	Password to decrypt with
#	sources	List of files to source in turns.
#
# Results:
#       Return the list of files that were successfully decrypted and
#       sourced.
#
# Side Effects:
#       None.
proc ::ruler::Source { slave paswd { sources {}} } {
    set success {}
    foreach fname $sources {
	::utils::debug INFO "Passing content of $fname to $slave for execution"
	set fd [open $fname {RDONLY BINARY}]
	set script [::crypt::decrypt [read $fd] $paswd]
	if { [catch {$slave eval $script} err] } {
	    ::utils::debug WARN "Error when evaluating $fname in slave: $err"
	} else {
	    lappend success $fname
	}
	close $fd
    }

    return $success
}


# ::ruler::Cache -- Find device specific cache dir
#
#       This will return the USB key specific cache dir, creating it
#       if it does not exist.
#
# Arguments:
#	dev	Path to device (of USB key).
#
# Results:
#       Return location of cache directory, empty on errors.
#
# Side Effects:
#       None.
proc ::ruler::Cache { dev } {
    if { [dict exists $vars::uuids $dev] } {
	set cdir [file join $vars::cache [dict get $vars::uuids $dev]]
	if { ![file isdirectory $cdir] } {
	    ::utils::debug NOTICE "Creating cache [file tail $cdir] for key\
                                   under $vars::cache"
	    file mkdir $cdir
	}
	return $cdir
    }
    return ""
}


# ::ruler::Watch -- Watch for key insertion/removal
#
#       This procedure is registered as the handler for insertion and
#       removal of all USB keys.  On insertion, it copies off rules
#       and execute on rules in a safe interpreter.  On removal, it
#       exectues the off rules that were cached at insertion.
#
# Arguments:
#	op	Operation on USB key (INSERT or REMOVE)
#	serial	Serial number of USB key.
#	dev	Device path of key
#	dir	Mount directory for key, i.e. where we can read/write files to it
#
# Results:
#       None.
#
# Side Effects:
#       See description
proc ::ruler::Watch { op serial dev dir } {
    set rdir [file join $dir [::crypt::hostid]]

    switch -glob -nocase -- $op {
	"INSERT*" {
	    # Access directory for key specific data on local host.
	    # We use a dictionary cache to map key devices to the UUID
	    # given away by linux as this information might already
	    # have disappeared on removal of the key.
	    dict set vars::uuids $dev [::disk::find uuid $dev]
	    set cdir [Cache $dev]

	    # Copy off rules to the cache.  We have to decrypt using
	    # the serial number of the USB key and re-encrypt using
	    # the internal password.
	    foreach fname [Files $rdir ${vars::-removal}] {
		set dst_fname [file join $cdir [file tail $fname]]
		::utils::debug INFO \
		    "Copying [file tail $fname] to cache at $cdir"
		set in [open $fname {RDONLY BINARY}]
		set dta [::crypt::decrypt [read $in] $serial]
		close $in

		set out [open $dst_fname {CREAT WRONLY BINARY TRUNC}]
		puts -nonewline $out [::crypt::encrypt $dta ${vars::-password}]
		close $out
	    }

	    # Creates a safe context and execute on rules in that context.
	    set slave [Safe $serial $cdir on off]
	    Source $slave $serial [Files $rdir ${vars::-insertion}]
	}
	"REM*" {
	    # Access directory for key specific data on local host.
	    # We use the dictionary cache created at insertion, and
	    # also arrange to remove the mapping from the dictionary.
	    set cdir [Cache $dev]
	    dict unset vars::uuids $dev
	    # Create a safe context, maybe killing the context created
	    # for on rules, then execute the off rules in that
	    # context.
	    set slave [Safe $serial $cdir off on]
	    Source $slave ${vars::-password} [Files $cdir ${vars::-removal}]
	}
    }
}

package provide ruler 0.1
